<?php
function tentukan_nilai($number)
{
    //  kode disini
    if ($number > 76) {
    	echo "Sangat baik";
    } else if ($number > 67) {
    	echo "Baik";
    } else if ($number >= 67) {
    	echo "Cukup";
    } else if ($number <= 43) {
    	echo "Kurang";
    }
    echo "<br>";
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>